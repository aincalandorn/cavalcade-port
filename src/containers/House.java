/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

import java.util.ArrayList;

/**
 *
 * @author 631734
 */
public class House {
/*
 * Updated: 20:16 11/30/2014
 */
	private int revealed = 0;
	private Card first;
	private Card second;
	private Card third;
	private Card spare;
	private boolean stagPresent;
	private int stagCard;

	public String getHand() {
				
		String ret = "<strong>The house's hand: </strong>";
		for(int x=0;x<1;x++){
			if (revealed > 0) {
				if(stagPresent&&revealed==stagCard){
					stagPresent=false;
					ret +=switchCard();
					x=0;
				}
				
				switch(revealed){
				case 1:
					ret += first.getCard() + ", [hidden], [hidden]";
					break;
				case 2:
					ret += first.getCard() + ", " + second.getCard() + ", [hidden]";
					break;
				case 3:
					ret += first.getCard() + ", " + second.getCard() + ", "
							+ third.getCard();
					break;
				default:
					System.out.println("Unknown revealed count?");
				}
			}//end revealed 
			else {
				ret += "[hidden], [hidden], [hidden]";
			}
		}
		return ret;
	}

	private String switchCard() {
		String ret ="";
			if (revealed == stagCard) {
				ret += "The Stag has been revealed and replaced!<br /><strong>";
				switch (stagCard) {
				case 1:
					first = spare;
					ret+="The new house hand is:";
					break;
				case 2:
					second = spare;
					ret+="The new house hand is:";
					break;
				case 3:
					third = spare;
					ret+="The new house hand is:";
					break;
				default:
					System.out.println("Unknown stag position!");
				}
			}//end switch
			ret+="</strong> ";
			return ret;
	}

	public void setCards(Card c1, Card c2, Card c3, Card s) {
		stagPresent=false;
		revealed=0;
		if (isStag(c1) || isStag(c2) || isStag(c3)) {
			stagPresent = true;
			if (isStag(c1)) {
				stagCard = 1;
			} else if (isStag(c2)) {
				stagCard = 2;
			} else if (isStag(c3)) {
				stagCard = 3;
			} else {
				stagCard = 0;
			}
		}
		first = c1;
		second = c2;
		third = c3;
		spare = s;
	}

	public void reveal(int reveal) {
		this.revealed=reveal;
	}

	public String allCards() {
		String ret = "The houses cards are: ";

		ret += first.getCard() + ", " + second.getCard() + ", "
				+ third.getCard();
		ret += "\n<br />The house's spare card is: " + spare.getCard();
		ret += "\n<br />The revealed card count is: "+revealed+"<br />";
		return ret;
	}

	private boolean isStag(Card c) {
		return c.isWildcard();
	}

	public House() {

	}
	public void revealAll() {
		for(int x = 0;x<4;x++){
			reveal(x);
			getHand();
		}
	}

	public ArrayList<Card> getCards(){
		ArrayList<Card> cards = new ArrayList<>();
		if(isStag(first)){
			cards.add(spare);
		}else{
			cards.add(first);
		}
		
		if(isStag(second)){
			cards.add(spare);
		}else{
			cards.add(second);
		}
	
		if(isStag(third)){
			cards.add(spare);
		}else{
			cards.add(third);
		}
		return cards;
	}
}
