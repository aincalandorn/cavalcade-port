/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

/**
 *
 * @author 631734
 */
public class Card {
    /*
        S1: Avatar, S2: Trickster, S3: Wanderer, S4: Stag (WILDCARD), S5: Dancer
        L1: Lady, L2: Champion, L3: Priestess, L4: Steed , L5:Maiden
        D1: Queen, D2: Slayer , D3: Zealot, D4: Beast, D5: Harlot
    */
    private String suit;
    private String name;
    private int num;
    
    public boolean isWildcard(){
        if(suit.charAt(0)=='S'&&num==4){
            return true;
        }
        else{
            return false;
        }
    }
    public String getCard(){
        String ret = ""+suit.charAt(0)+num+": "+name;
        if(isWildcard()){
            ret+= " <strong>[*]</strong>";
        }
        return ret;
    }
    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Card(String suit, String name, int num) {
        this.suit = suit.toUpperCase();
        this.name = name;
        this.num = num;
    }
	@Override
	public String toString() {
		return "[" + suit + "-" + num + "]";
	}
    
}
