/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

import java.util.ArrayList;


/**
 *
 * @author 631734
 */
public class Player implements Comparable<Player> {
    private String name;
    private int coins;
    private boolean out=false;
    private boolean isPlayer=false;
    private Card first;
    private Card second;
    private double score = 0;
    public String getHand(){
        String ret = "<strong>";
        if(isPlayer){
            ret+="Your hand: ";
        }
        else{
            ret+=name+"'s hand: ";
        }
        ret+="</strong>";
        ret+=first.getCard()+", "+second.getCard();
        return ret+"\n";
    }
    public double getScore(){
    	return score;
    }
    public void setScore(double newScore){
    	score=newScore;
    }
    public void setOut(boolean isOut){
        out=isOut;
        if(isOut){
        	score=-1000;
        }
    }
    public boolean isOut(){
        return out;
    }
    public String getName(){
        return name;
    }
    public int getCoins(){
        return coins;
    }
    public void resetCoins(){
        coins=45;
    }
    public void changeCoins(int factor){
        coins+=factor;
        if(coins<0){
            coins=0;
        }
    }
    public Player(String name) {
        this.name = name;
        this.coins = 45;
    }
    public Player(String name, boolean isPlayer) {
        this.name = name;
        this.coins = 45;
        this.isPlayer = true;
    }
    public void setCards(Card c1, Card c2) {
        first = c1;
        second = c2;
        score=0;
    }
    
    public ArrayList<Card> getCards(){
		ArrayList<Card> cards = new ArrayList<>();
		cards.add(first);
		cards.add(second);
		return cards;
	}
	@Override
	public int compareTo(Player ref) {
		if(out){
			return -2;
		}
		else if(ref.getScore()>score){
			return 1;
		}
		else if(ref.getScore()==score){
			return 0;
		}
		else{
			return -1;
		}
	}
	@Override
	public String toString() {
		String ret = name;
		if(out){
			ret+=" (out)";
		}
		ret+="[coins=" + coins + ", score=" + score + "]";
		
		return ret;
	}
}
