package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import containers.Card;
import containers.House;
import containers.Player;

import java.awt.Component;

import javax.swing.Box;

@SuppressWarnings("unused")
public class GameGUI extends JFrame implements Observer {
	/**
	 * Updated: 20:16 11/30/2014
	 */
	private static final long serialVersionUID = 5190072073769289887L;

	private Game game;
	private Thread t;
	protected boolean debug;
	protected PrintStream out = System.out;
	protected JPanel contentPane;
	protected JPanel gamePanel;
	protected JEditorPane gamePane;
	protected JLabel currRound;
	protected JLabel coins_pot;
	protected JLabel coins_pc;
	protected JLabel name_pc;
	protected JLabel coins_p3;
	protected JLabel coins_p2;
	protected JLabel coins_p1;

	protected int pot = 0;
	protected int round = 1;

	protected Player p_p = new Player("Player", true);
	protected Player p_1 = new Player("Alder");
	protected Player p_2 = new Player("Del");
	protected Player p_3 = new Player("Ain");
	protected House h = new House();

	protected final String HANDS = "The scoring is as follows: least is the pair,"
			+ "after that comes two pairs, then threekind, followed by the "
			+ "mixed Cavalcade. That last one is when you have the cards one"
			+ " through five of any suits. Then comes full house - your hand"
			+ " - followed by a mixed flush. This is when you have four cards"
			+ " of the same suit. Next is fourkind - a hand only possibly "
			+ "with the stag - and lastly full Cavalcade, which is when you"
			+ " have all the cards of one suit.";
	protected final String HANDS_2 = "Finally, a hand with the stag is not necessarily a win. "
			+ "If I have a hand that evaluates to a threekind, and you have"
			+ " a pair plus the stag, I will win the hand even if your "
			+ "threekind happens to be better than mine. The Shadow Stag can"
			+ " take on many forms, but is thus weaker than a pure hand.";

	private JButton btnRaise;

	private JButton btnCall;

	private JButton btnFold;
	private JButton btnNextRound;

	private JButton btnNext;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					boolean setDebug = false;
					if (args.length > 0 && args[0].equals("debug")) {
						setDebug = true;
						System.out.println("****Debugging enabled!!!****");
					}
					GameGUI gui = new GameGUI(setDebug);
					if (!setDebug) {
						if (JOptionPane
								.showConfirmDialog(
										null,
										"Welcome to Cavalcade. Do you wish to see the instructions first?\nThe instructions can be viewed at any time by clicking the instructions button.",
										"Welcome!", JOptionPane.YES_NO_OPTION,
										JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
							gui.showInstructionsBox(gui);
						}
					}
					gui.setVisible(true);
					gui.startGame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected void startGame() {
		game = new Game();
		Thread t = new Thread(game);
		game.addObserver(this);
		t = new Thread(game);
		t.start();

	}

	protected void setGame(Game game2) {
		game = game2;

	}

	/**
	 * Create the frame.
	 */
	public GameGUI(boolean _debug) {
		debug = _debug;
		// gui
		setResizable(false);
		setTitle("Cavalcade");
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		JFrame frame = this;
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				confirmExit(frame);
			}
		});

		setBounds(100, 100, 824, 524);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel aiPlayerPanel = new JPanel();
		contentPane.add(aiPlayerPanel, BorderLayout.EAST);
		GridBagLayout gbl_aiPlayerPanel = new GridBagLayout();
		gbl_aiPlayerPanel.columnWidths = new int[] { 100, 0 };
		gbl_aiPlayerPanel.rowHeights = new int[] { 25, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0 };
		gbl_aiPlayerPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_aiPlayerPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		aiPlayerPanel.setLayout(gbl_aiPlayerPanel);

		JLabel name_p1 = new JLabel(p_1.getName());
		name_p1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		name_p1.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_name_p1 = new GridBagConstraints();
		gbc_name_p1.insets = new Insets(0, 0, 5, 0);
		gbc_name_p1.fill = GridBagConstraints.BOTH;
		gbc_name_p1.gridx = 0;
		gbc_name_p1.gridy = 0;
		aiPlayerPanel.add(name_p1, gbc_name_p1);

		coins_p1 = new JLabel(p_1.getCoins() + " coins");
		GridBagConstraints gbc_coins_p1 = new GridBagConstraints();
		gbc_coins_p1.insets = new Insets(0, 0, 5, 0);
		gbc_coins_p1.gridx = 0;
		gbc_coins_p1.gridy = 1;
		aiPlayerPanel.add(coins_p1, gbc_coins_p1);

		JLabel name_p2 = new JLabel(p_2.getName());
		name_p2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_name_p2 = new GridBagConstraints();
		gbc_name_p2.insets = new Insets(0, 0, 5, 0);
		gbc_name_p2.gridx = 0;
		gbc_name_p2.gridy = 2;
		aiPlayerPanel.add(name_p2, gbc_name_p2);

		coins_p2 = new JLabel(p_2.getCoins() + " coins");
		GridBagConstraints gbc_coins_p2 = new GridBagConstraints();
		gbc_coins_p2.insets = new Insets(0, 0, 5, 0);
		gbc_coins_p2.gridx = 0;
		gbc_coins_p2.gridy = 3;
		aiPlayerPanel.add(coins_p2, gbc_coins_p2);

		JLabel name_p3 = new JLabel(p_3.getName());
		name_p3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_name_p3 = new GridBagConstraints();
		gbc_name_p3.insets = new Insets(0, 0, 5, 0);
		gbc_name_p3.gridx = 0;
		gbc_name_p3.gridy = 4;
		aiPlayerPanel.add(name_p3, gbc_name_p3);

		coins_p3 = new JLabel(p_3.getCoins() + " coins");
		GridBagConstraints gbc_coins_p3 = new GridBagConstraints();
		gbc_coins_p3.insets = new Insets(0, 0, 5, 0);
		gbc_coins_p3.gridx = 0;
		gbc_coins_p3.gridy = 5;
		aiPlayerPanel.add(coins_p3, gbc_coins_p3);

		name_pc = new JLabel(p_p.getName());
		name_pc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_name_pc = new GridBagConstraints();
		gbc_name_pc.insets = new Insets(0, 0, 5, 0);
		gbc_name_pc.gridx = 0;
		gbc_name_pc.gridy = 6;
		aiPlayerPanel.add(name_pc, gbc_name_pc);

		coins_pc = new JLabel(p_p.getCoins() + " coins");
		GridBagConstraints gbc_coins_pc = new GridBagConstraints();
		gbc_coins_pc.insets = new Insets(0, 0, 5, 0);
		gbc_coins_pc.gridx = 0;
		gbc_coins_pc.gridy = 7;
		aiPlayerPanel.add(coins_pc, gbc_coins_pc);

		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut.gridx = 0;
		gbc_verticalStrut.gridy = 8;
		aiPlayerPanel.add(verticalStrut, gbc_verticalStrut);

		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut_1.gridx = 0;
		gbc_verticalStrut_1.gridy = 9;
		aiPlayerPanel.add(verticalStrut_1, gbc_verticalStrut_1);

		JLabel lblPot = new JLabel("Pot");
		lblPot.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblPot = new GridBagConstraints();
		gbc_lblPot.insets = new Insets(0, 0, 5, 0);
		gbc_lblPot.gridx = 0;
		gbc_lblPot.gridy = 10;
		aiPlayerPanel.add(lblPot, gbc_lblPot);

		coins_pot = new JLabel(pot + " coins");
		GridBagConstraints gbc_coins_pot = new GridBagConstraints();
		gbc_coins_pot.insets = new Insets(0, 0, 5, 0);
		gbc_coins_pot.gridx = 0;
		gbc_coins_pot.gridy = 11;
		aiPlayerPanel.add(coins_pot, gbc_coins_pot);

		JLabel lblRound = new JLabel("Round");
		lblRound.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblRound = new GridBagConstraints();
		gbc_lblRound.insets = new Insets(0, 0, 5, 0);
		gbc_lblRound.gridx = 0;
		gbc_lblRound.gridy = 13;
		aiPlayerPanel.add(lblRound, gbc_lblRound);

		currRound = new JLabel("#" + round);
		GridBagConstraints gbc_currRound = new GridBagConstraints();
		gbc_currRound.gridx = 0;
		gbc_currRound.gridy = 14;
		aiPlayerPanel.add(currRound, gbc_currRound);

		JPanel controlPanel = new JPanel();
		contentPane.add(controlPanel, BorderLayout.SOUTH);

		btnNext = new JButton("Next Round");
		btnNext.addActionListener((ActionEvent ae) -> game.newRound());
		btnNext.setEnabled(false);
		controlPanel.add(btnNext);

		btnRaise = new JButton("Raise");
		btnRaise.addActionListener((ActionEvent ae) -> game.raise());
		controlPanel.add(btnRaise);

		btnCall = new JButton("Call");
		btnCall.addActionListener((ActionEvent ae) -> game.call());
		controlPanel.add(btnCall);

		btnFold = new JButton("Fold");
		btnFold.addActionListener((ActionEvent ae) -> game.fold());
		controlPanel.add(btnFold);

		JButton btnQuit = new JButton("Quit");
		btnQuit.addActionListener((ActionEvent ae) -> confirmExit(frame));
		controlPanel.add(btnQuit);

		JButton btnInst = new JButton("Instructions");
		btnInst.addActionListener((ActionEvent ae) -> showInstructionsBox(frame));
		controlPanel.add(btnInst);

		gamePanel = new JPanel();
		contentPane.add(gamePanel, BorderLayout.CENTER);

		gamePane = new JEditorPane();
		gamePane.setEditable(false);
		gamePane.setContentType("text/html");
		gamePane.setPreferredSize(new Dimension(650, 400));
		gamePane.setText("Welcome!");
		JScrollPane jsp = new JScrollPane(gamePane);
		gamePanel.add(jsp);
	}

	private void showInstructionsBox(JFrame frame) {
		JOptionPane.showMessageDialog(frame,
				"<html><body>" + "<p style=\"width:400px;\">" + HANDS
						+ "</p><br />" + "<p style=\"width:400px;\">" + HANDS_2
						+ "</p></body></html>", "Instructions",
				JOptionPane.PLAIN_MESSAGE);
	}

	private void confirmExit(JFrame frame) {
		if (JOptionPane.showConfirmDialog(frame,
				"Are you sure to close this window?", "Really Closing?",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	@Override
	public void update(Observable arg0, Object text) {
		String setText = (String) text;
		setText.replaceAll("\n", "<br />");

		gamePane.setText(setText);
	}

	public class Game extends Observable implements Runnable {
		private ArrayList<Card> cards;
		private PrintStream out = System.out;
		private Stack<Card> deck;

		private String previousAction = "";
		private int multi = 1;
		private final int BASE = 5;

		private int npcOut = 0;
		private boolean gameOver;
		private boolean fold;

		public void push(String msg) {
			this.setChanged();
			this.notifyObservers("<html><body>" + msg + "</body></html>");
		}

		public void newRound() {
			round = 1;
			toggleButtons(false);
			newGame();
		}

		public void fold() {
			round = 4;
			updateGui();
			processTurn();
			previousAction = "You folded!<hr />";
			fold = true;
			h.revealAll();
			showHands(true);
		}

		public Game() {
			cards = new ArrayList<Card>();
			cards.add(new Card("Shadow", "Avatar", 1));
			cards.add(new Card("Shadow", "Trickster", 2));
			cards.add(new Card("Shadow", "Wanderer", 3));
			cards.add(new Card("Shadow", "Stag", 4));
			cards.add(new Card("Shadow", "Dancer", 5));

			cards.add(new Card("Light", "Lady", 1));
			cards.add(new Card("Light", "Champion", 2));
			cards.add(new Card("Light", "Priestess", 3));
			cards.add(new Card("Light", "Steed", 4));
			cards.add(new Card("Light", "Maiden", 5));

			cards.add(new Card("Dark", "Queen", 1));
			cards.add(new Card("Dark", "Slayer", 2));
			cards.add(new Card("Dark", "Zealot", 3));
			cards.add(new Card("Dark", "Beast", 4));
			cards.add(new Card("Dark", "Harlot", 5));
		}

		@Override
		public void run() {
			newGame();
		}

		public void newGame() {
			checkOut();
			if (!gameOver) {
				resetDeck();
				buildDeck();
				shuffleDeck();
				initialBet();
				deal();
				showHands(false);
			} else {
				updateGui();
			}
		}

		private void resetDeck() {
			deck = new Stack<>();
		}

		private void buildDeck() {
			deck.addAll(cards);
		}

		private void shuffleDeck() {
			Collections.shuffle(deck);
		}

		private void call() {
			previousAction = "You called!<hr />";
			processTurn();
		}

		private void raise() {
			int change = (multi * BASE);
			previousAction = "You raised!<hr />";
			p_p.changeCoins((-1) * change);
			pot += change;
			if (!p_1.isOut()) {
				p_1.changeCoins((-1) * change);
				pot += change;
			}
			if (!p_2.isOut()) {
				p_2.changeCoins((-1) * change);
				pot += change;
			}
			if (!p_3.isOut()) {
				p_3.changeCoins((-1) * change);
				pot += change;
			}
			processTurn();
		}

		private void processTurn() {
			if (round < 4) {
				h.reveal(round);
				round++;
				showHands(false);
			} else if (round == 4) {
				toggleButtons(true);
				h.reveal(3);
				showHands(true);
			}
		}

		private void toggleButtons(boolean b) {
			btnCall.setEnabled(!b);
			btnFold.setEnabled(!b);
			btnRaise.setEnabled(!b);
			btnNext.setEnabled(b);
			updateGui();

		}

		private void initialBet() {
			int change = (multi * BASE);

			p_p.changeCoins((-1) * change);
			pot += change;
			if (!p_1.isOut()) {
				p_1.changeCoins((-1) * change);
				pot += change;
			}
			if (!p_2.isOut()) {
				p_2.changeCoins((-1) * change);
				pot += change;
			}
			if (!p_3.isOut()) {
				p_3.changeCoins((-1) * change);
				pot += change;
			}

			updateGui();
		}

		private void updateGui() {
			updateCoins();
			currRound.setText("#" + round);
		}

		private void updateCoins() {
			coins_pc.setText(p_p.getCoins() + " coins");
			if (p_1.isOut()) {
				coins_p1.setText("<html><body><strong>Out</strong></body></html>");
			} else {
				coins_p1.setText(p_1.getCoins() + " coins");
			}

			if (p_2.isOut()) {
				coins_p2.setText("<html><body><strong>Out</strong></body></html>");
			} else {
				coins_p2.setText(p_2.getCoins() + " coins");
			}

			if (p_3.isOut()) {
				coins_p3.setText("<html><body><strong>Out</strong></body></html>");
			} else {
				coins_p3.setText(p_3.getCoins() + " coins");
			}

			coins_pot.setText(pot + " coins");
		}

		private void deal() {
			p_p.setCards(deck.pop(), deck.pop());
			if (!p_1.isOut()) {
				p_1.setCards(deck.pop(), deck.pop());
			}
			if (!p_1.isOut()) {
				p_2.setCards(deck.pop(), deck.pop());
			}
			if (!p_1.isOut()) {
				p_3.setCards(deck.pop(), deck.pop());
			}
			h.setCards(deck.pop(), deck.pop(), deck.pop(), deck.pop());
		}

		private void showHands(boolean endGame) {
			String setText = "";
			if (!previousAction.equals("")) {
				setText += previousAction;
				previousAction = "";
			}
			if (endGame) {
				setText += victor();
			} else {
				setText += p_p.getHand() + "\n<br />";
				setText += h.getHand();
			}

			setText = "<p>" + setText + "</p>";
			if (debug) {
				setText += getDebug();
			}
			push(setText);
			updateGui();
		}

		private String victor() {
			String ret = "";
			ret += h.getHand() + "<br />";

			HandCounter hcp = new HandCounter(p_p.getCards(), h.getCards());
			p_p.setScore(hcp.getScore());
			String hand_p = hcp.getHand();

			ret += p_p.getHand() + "<br />---" + hand_p + "<br />";

			if (!p_1.isOut()) {
				HandCounter hc1 = new HandCounter(p_1.getCards(), h.getCards());
				p_1.setScore(hc1.getScore());
				String hand_1 = hc1.getHand();
				ret += p_1.getHand() + "<br />---" + hand_1 + "<br />";
			}
			if (!p_2.isOut()) {
				HandCounter hc2 = new HandCounter(p_2.getCards(), h.getCards());
				p_2.setScore(hc2.getScore());
				String hand_2 = hc2.getHand();
				ret += p_2.getHand() + "<br />---" + hand_2 + "<br />";
			}
			if (!p_3.isOut()) {
				HandCounter hc3 = new HandCounter(p_3.getCards(), h.getCards());
				p_3.setScore(hc3.getScore());
				String hand_3 = hc3.getHand();
				ret += p_3.getHand() + "<br />---" + hand_3 + "<br />";
			}

			ret += "<br /><br />";
			ret += getWinner();
			return ret;
		}

		private String getWinner() {
			int winners = 0;
			String winnerNames = "";
			String ret = "";
			ArrayList<Player> players = new ArrayList<>();
			if(!fold){
				players.add(p_p);
			}
			if(!p_1.isOut()){
				players.add(p_1);
			}
			if(!p_2.isOut()){
				players.add(p_2);
			}
			if(!p_3.isOut()){
				players.add(p_3);
			}
			
			Player temp;
			if (players.size() > 1) {
				for (int x = 0; x < players.size(); x++) // bubble sort outer
				{
					for (int i = 0; i < players.size() - x - 1; i++) {
						if (players.get(i).compareTo(players.get(i + 1)) > 0) {
							temp = players.get(i);
							players.set(i, players.get(i + 1));
							players.set(i + 1, temp);
						}
					}
				}
			}
			double max = -100;
			// find highest
			for (Player p : players) {
				if (p.getScore() > max) {
					max = p.getScore();
				}
			}
			ArrayList<Player> win = new ArrayList<>();
			
			for (Player p : players) {
				if (p.getScore() == max) {
					if (winners > 0) {
						if(winners==2){
							winnerNames+=" and ";
						}
						else if(win.size()==(winners-1)){
							winnerNames+=", and ";
						}
						else{
							winnerNames += ", ";
						}
					}
					winners++;
					win.add(p);
					winnerNames += p.getName();
				}
			}
			
			// output
			if (winners == 0) {
				ret += "No winner. Pot will remain at the current amount.";
			} else {
				ret += "The winner";
				if (winners > 1) {
					ret += "s are ";
				} else {
					ret += " is ";
				}
				ret += winnerNames+". <br />";
				int change=(pot-(pot/winners)%5)/winners;
				int x = 0;
				if(winners>1){
					ret+= " The pot has been split among winners.";
				}
				for (Player p : win) {
					if (p.getName().equals(p_p.getName())) {
						pot-=change;
						p_p.changeCoins(change);
					} else if (p.getName().equals(p_1.getName())) {
						pot-=change;
						p_1.changeCoins(change);
					} else if (p.getName().equals(p_2.getName())) {
						pot-=change;
						p_2.changeCoins(change);
					} else if (p.getName().equals(p_3.getName())) {
						pot-=change;
						p_3.changeCoins(change);
					}
				}
			}
			return ret;
		}

		private void checkOut() {
			if (p_p.getCoins() == 0) {
				gameOver(true);
			} else {
				if (!p_1.isOut() && p_1.getCoins() == 0) {
					p_1.setOut(true);
					npcOut++;
					multi++;
				}
				if (!p_2.isOut() && p_2.getCoins() == 0) {
					p_2.setOut(true);
					npcOut++;
					multi++;
				}
				if (!p_3.isOut() && p_3.getCoins() == 0) {
					p_3.setOut(true);
					npcOut++;
					multi++;
				}
			}
			
			if (npcOut == 3) {
				gameOver(true);
			}
		}

		private void gameOver(boolean playerLast) {
			btnRaise.setEnabled(false);
			btnCall.setEnabled(false);
			btnFold.setEnabled(false);
			btnNext.setEnabled(false);
			
			gameOver = true;
			if (playerLast) {
				push("You won, congrats!<br />To play again, please restart the program.");
			} else {

				push("You lost, better luck next time!<br />To play again, please restart the program.");
				
			}

		}

		private String getDebug() {
			String ret = "<div style=\"background:black;color:red;\">----";
			ret += "<br />Debugging: <br />";
			// do not add above this line

			// count scores
			System.out.print("Player's cards: ");
			HandCounter hcp = new HandCounter(p_p.getCards(), h.getCards());
			double score_p = hcp.getScore();
			String hand_p = hcp.getHand();
			ret += p_p.getHand() + " (" + score_p
					+ " points, hand: <font color=\"yellow\">" + hand_p
					+ "</font>)<br />";
			if (!p_1.isOut()) {
				System.out.print(p_1.getName()+"'s cards: ");
				HandCounter hc1 = new HandCounter(p_1.getCards(), h.getCards());
				double score_1 = hc1.getScore();
				String hand_1 = hc1.getHand();
				ret += p_1.getHand() + " (" + score_1
						+ " points, hand: <font color=\"yellow\">" + hand_1
						+ "</font>)<br />";
			} else {
				ret += p_1.getName() + " is out.<br />";
			}
			if (!p_2.isOut()) {
				System.out.print(p_2.getName()+"'s cards: ");
				HandCounter hc2 = new HandCounter(p_2.getCards(), h.getCards());
				double score_2 = hc2.getScore();
				String hand_2 = hc2.getHand();
				ret += p_2.getHand() + " (" + score_2
						+ " points, hand: <font color=\"yellow\">" + hand_2
						+ "</font>)<br />";
			} else {
				ret += p_2.getName() + " is out.<br />";
			}
			if (!p_3.isOut()) {
				System.out.print(p_3.getName()+"'s cards: ");
				HandCounter hc3 = new HandCounter(p_3.getCards(), h.getCards());
				double score_3 = hc3.getScore();
				String hand_3 = hc3.getHand();
				ret += p_3.getHand() + " (" + score_3
						+ " points, hand: <font color=\"yellow\">" + hand_3
						+ "</font>)<br />";
			} else {
				ret += p_3.getName() + " is out.<br />";
			}

			ret += h.allCards() + "<br />";
			// do not add below this
			ret += "----</div>";

			return ret;
		}
	}
}
