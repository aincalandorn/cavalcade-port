package gui;

import java.util.ArrayList;

import containers.Card;

public class HandCounter {
	private ArrayList<Card> cards;
	private boolean hasStag = false;
	private double score = 0;
	private String hand = "Nothing!";

	public HandCounter(ArrayList<Card> playerCards, ArrayList<Card> houseCards) {
		cards = new ArrayList<>();
		cards.addAll(playerCards);
		cards.addAll(houseCards);
		for (Card c : cards) {
			if (c.isWildcard()) {
				hasStag = true;
			}
		}
		System.out.println(cards);
		calculateScore();
	}

	private void calculateScore() {

		// similar cards...
		ArrayList<NumVal> counts = new ArrayList<NumVal>();
		;

		for (Card c : cards) {
			if (!c.isWildcard()) {

				int cardVal = c.getNum();
				boolean foundVal = false;
				for (NumVal n : counts) {
					if (cardVal == n.val) {
						n.num++;
						foundVal = true;
						break;
					}
				}
				if (!foundVal) {
					counts.add(new NumVal(1, cardVal));
				}
			}
		}
		ArrayList<NumVal> tmp = new ArrayList<>();
		tmp.addAll(sortVal(counts));
		counts.clear();
		counts.addAll(tmp);
		tmp = null;

		ArrayList<NumSuit> suits = new ArrayList<NumSuit>();
		for (Card c : cards) {
			if (c.isWildcard())
				continue; // what exactly does this do?

			String cardSuit = c.getSuit();
			boolean foundSuit = false;
			for (NumSuit n : suits) {
				if (cardSuit.equals(n.suit)) {
					n.num++;
					foundSuit = true;
					break;
				}
			}
			if (!foundSuit) {
				suits.add(new NumSuit(1, cardSuit));
			}
		}
		ArrayList<NumSuit> tmp2 = new ArrayList<>();
		tmp2.addAll(sortSuit(suits));
		suits.clear();
		suits.addAll(tmp2);
		tmp2 = null;

		if (hasStag) {
			for (NumVal n : counts) {
				n.num++;
			}
			for (NumSuit n : suits) {
				n.num++;
			}
		}

		if (suits.get(0).num == 5) {
			score = 7;
			hand = "Full Cavalcade of " + suits.get(0).suit;
			if (suits.get(0).suit.equals("Shadow")) {
				hasStag = false;
			}
		} else if (counts.get(0).num == 4) {
			score = 6;
			hand = "Four kind of " + counts.get(0).val;
		} else if (suits.get(0).num == 4) {
			score = 5;
			hand = "Partial flush of " + suits.get(0).suit;
			if (!suits.get(0).suit.equals("Shadow")) {
				hasStag = false;
			}
		} else if (counts.get(0).num == 3 && counts.get(1).num == 2) {
			score = 4;
			int high;
			int low;

			if (counts.get(0).val > counts.get(1).val) {
				high = counts.get(0).val;
				low = counts.get(1).val;
			} else {
				high = counts.get(1).val;
				low = counts.get(0).val;
			}

			hand = "Full house of " + low + " and " + high;
		} else if (counts.size() == 5 || (counts.size() == 4 && hasStag)) {
			score = 3;
			hand = "Mixed Cavalcade";
		} else if (counts.get(0).num == 3) {
			score = 2;
			hand = "Threekind";
		} else if (counts.get(0).num == 2 && counts.get(0).num == 2) {
			score = 1;
			int high;
			int low;

			if (counts.get(0).val > counts.get(1).val) {
				high = counts.get(0).val;
				low = counts.get(1).val;
			} else {
				high = counts.get(1).val;
				low = counts.get(0).val;
			}

			hand = "Two pair of " + low + " and " + high;
		} else if (counts.get(0).num == 2) {
			score = 0;
			hand = "Pair of " + counts.get(0).val;
		} else {
			hand = "Nothing!";
			score = -2;
		}

		if (hasStag) {
			score -= 0.5;
			hand += " (Stag)";
		}
		if (score < -2) {
			score = -2;
		}
	}

	private ArrayList<NumVal> sortVal(ArrayList<NumVal> toSort) {
		NumVal[] comparable = new NumVal[toSort.size()];
		toSort.toArray(comparable);

		boolean changed = false;
		do {
			changed = false;
			for (int a = 0; a < comparable.length - 1; a++) {
				if (comparable[a].compareTo(comparable[a + 1]) > 0) {
					NumVal tmp = comparable[a];
					comparable[a] = comparable[a + 1];
					comparable[a + 1] = tmp;
					changed = true;
				}
			}
		} while (changed);

		ArrayList<NumVal> ret = new ArrayList<>();
		for (NumVal n : comparable) {
			ret.add(n);
		}
		return ret;
	}

	private ArrayList<NumSuit> sortSuit(ArrayList<NumSuit> toSort) {
		NumSuit[] comparable = new NumSuit[toSort.size()];
		toSort.toArray(comparable);

		boolean changed = false;
		do {
			changed = false;
			for (int a = 0; a < comparable.length - 1; a++) {
				if (comparable[a].compareTo(comparable[a + 1]) > 0) {
					NumSuit tmp = comparable[a];
					comparable[a] = comparable[a + 1];
					comparable[a + 1] = tmp;
					changed = true;
				}
			}
		} while (changed);

		ArrayList<NumSuit> ret = new ArrayList<>();
		for (NumSuit n : comparable) {
			ret.add(n);
		}
		return ret;
	}

	public double getScore() {
		return score;
	}

	public String getHand() {
		return hand;
	}

	private class NumVal implements Comparable<NumVal> {
		public int num;
		public int val;

		@Override
		public String toString() {
			return "NumVal [num=" + num + ", val=" + val + "]";
		}

		public NumVal(int num, int val) {
			this.num = num;
			this.val = val;
		}

		@Override
		public int compareTo(NumVal ref) {
			if (ref.num > num) {
				return 1;
			} else if (ref.num < num) {
				return -1;
			} else {
				if (ref.val > val) {
					return 1;
				} else {
					return -1;
				}
			}
		}
	}

	private class NumSuit implements Comparable<NumSuit> {
		public int num;
		public String suit;

		@Override
		public String toString() {
			return "NumSuit [num=" + num + ", suit=" + suit + "]";
		}

		public NumSuit(int num, String suit) {
			this.num = num;
			this.suit = suit;
		}

		@Override
		public int compareTo(NumSuit ref) {
			if (ref.num > num) {
				return 1;
			} else if (ref.num < num) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}
